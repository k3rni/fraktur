# Fraktur keyboard for X11

This is a partial XKB keyboard description file, allowing you to type [Unicode Mathematical Fraktur](http://www.unicode.org/charts/PDF/U1D400.pdf)-block characters directly from your keyboard. It's not a full variant file, so setting it up is somewhat arcane.

# Installation

Check this repository out somewhere, for example to `~/.xkb` (this will be used in following examples). 

If you don't use any custom XKB modifications, you should be fine with what proceeds. But if you do, just merge the symbols list with what `setxkbmap -query -verbose` prints in the symbols line, adding the correct layout number to fraktur.

Check how your keyboard is configured. You'll probably get something like this. My default keyboard is Polish, yours may be different. It doesn't matter.

```
$ setxkbmap -query -verbose

keycodes:   evdev+aliases(qwerty)
types:      complete
compat:     complete
symbols:    pc+pl+inet(evdev)
geometry:   pc(pc105)
rules:      evdev
model:      pc105
layout:     pl
```

Remember the symbols output, this is what we need from now on. We'll add a second layout, which will be US, with additional Fraktur keys loaded.


```
$ setxkbmap -I~/.xkb -symbols 'pc+pl+us:2+inet(evdev)+group(alt_shift_toggle)+fraktur:2
```

Change the language to what you normally use. Now your keyboard should work as before, but pressing <kbd>Alt+Shift</kbd> together changes it to Fraktur-mode. Pressing it again returns to normal operation. 

### Install system-wide

Instead of invoking `setxkbmap -I~/.xkb` you can just put the `symbols/fraktur` file into `/usr/share/X11/xkb/symbols`. This will make it available to any `setxkbmap` invocation from now on, and you can safely delete this project from your system.

You may need to run `dpkg-reconfigure xkb-data` and restart Xorg afterwards. If you're not on Debian/Ubuntu, just `sudo rm /var/lib/xkb/*.xkm` instead.

## Switching layouts

You have a couple more options for switching. To list them, do:

```
$ grep grp: /usr/share/X11/xkb/rules/base.lst
```

Use one of these instead of `group(alt_shift_toggle)` (without the `grp:prefix`) to change how you flip layouts. For some fun, use `caps_toggle`, which will make your <kbd>Capslock</kbd> key an <kbd>Anschluss</kbd> key.

## Setting as default

Add the `setxkbmap` command to your startup scripts (e.g. `.xsession` or your desktop environment's config) to preserve it.
